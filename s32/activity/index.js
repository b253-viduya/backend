const http = require('http');

const server = http.createServer((req, res) => {
    if (req.url === '/' && req.method == "GET") {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.write('Welcome to Booking System');
        res.end();
    } else if (req.url === '/profile' && req.method == "GET") {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.write('Welcome to your profile!');
        res.end();
    } else if (req.url === '/courses' && req.method == "GET") {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.write('Here’s our courses available');
        res.end();
    } else if (req.url === '/addCourse' && req.method == "POST") {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.write('Add a course to our resources');
        res.end();
    } else if (req.url === '/updateCourse' && req.method == "PUT") {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.write('Update a course to our resources');
        res.end();
    } else if (req.url === '/archiveCourses' && req.method == "DELETE") {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.write('Archive courses to our resources');
        res.end();
    } else {
        res.writeHead(404, {
            'Content-Type': 'text/plain'
        });
        res.write('404 Not Found');
        res.end();
    }
});

const port = 4000;

server.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
});