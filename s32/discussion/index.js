let http = require("http");

let port = 4000;

http.createServer(function (request, response) {

    if (request.url == "/items" && request.method == "GET") {

        // The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
        // The method "GET" means that we will be retrieving or reading information
        response.writeHead(200, {
            "Content-Type": "text/plain"
        });
        // Ends the response process
        response.end("Data Retrieved from the database");

        // The method "POST" means that we will be adding or creating information
        // In this example, we will just be sending a text response for now
    } else if (request.url == "/items" && request.method == "POST") {
        // Requests the "/items" path and "SENDS" information
        response.writeHead(200, {
            "Content-Type": "text/plain"
        });
        response.end("Data to be send to the Database");
    }

}).listen(port);

console.log(`Running at local ${port}`)