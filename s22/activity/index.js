/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function called register which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, return the message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and return the message:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

function register(username) {
    if (registeredUsers.includes(username)) {
        return "Registration failed. Username already exists!";
    } else {
        registeredUsers.push(username);
        return "Thank you for registering!";
    }
}



/*
    2. Create a function called addFriend which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then return the message with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, return the message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

function addFriend(username) {
    if (registeredUsers.includes(username)) {
        friendsList.push(username);
        return `You have added ${username} as a friend!`;
    } else {
        return "User not found.";
    }
}


/*
    3. Create a function called displayFriends which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty return the message: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function displayFriends() {
    if (friendsList.length === 0) {
        return "You currently have 0 friends. Add one first.";
    } else {
        friendsList.forEach(friend => console.log(friend));
    }
}


/*
    4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty return the message:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty return the message:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function displayNumberOfFriends() {
    if (friendsList.length === 0) {
        return "You currently have 0 friends. Add one first.";
    } else {
        return `You currently have ${friendsList.length} friends.`;
    }
}


/*
    5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty return a message:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

function deleteFriend() {
    if (friendsList.length === 0) {
        return "You currently have 0 friends. Add one first.";
    } else {
        const deletedFriend = friendsList.pop();
        return `You have deleted ${deletedFriend} from your friends list.`;
    }
}


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

function SGdeleteFriend(username) {
    if (friendsList.length === 0) {
        return "You currently have 0 friends. Add one first.";
    } else {
        const index = friendsList.indexOf(username);
        if (index === -1) {
            return "User not found.";
        } else {
            const deletedFriend = friendsList.splice(index, 1)[0];
            return `You have deleted ${deletedFriend} from your friends list.`;
        }
    }
}



//For exporting to test.js
try {
    module.exports = {
        registeredUsers,
        friendsList,
        register,
        addFriend,
        displayFriends,
        displayNumberOfFriends,
        deleteFriend
    }
} catch (err) {

}