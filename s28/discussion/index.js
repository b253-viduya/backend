// CRUD Operations
/*
		    - CRUD operations are the heart of any backend application.
		    - Mastering the CRUD operations is essential for any developer.
		    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
		    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
		
            Syntax:

            Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

		Insert Many Documents
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			]);
        
        
        
            */

// Insert documents (create)
db.users.insertOne({
    "firstName": "Jeroxzi",
    "lastName": "Viduya",
    "mobileNumber": "09123456789",
    "email": "jeroxzi16@gmail.com",
    "company": "Zuitt"
});

db.user.insertMany([{
        "firstName": "Stephen",
        "lastName": "Hawking",
        age: 76,
        contact: {
            phone: "987654321",
            email: "stephenhawking@mail.com"
        },
        courses: ["Python", "React", "PHP", "CSS"],
        department: "none"
    },
    {
        "firstName": "Nel",
        "lastName": "Armtrong",
        age: 82,
        contact: {
            phone: "987654321",
            email: "neil@mail.com"
        },
        courses: ["Python", "React", "PHP", "CSS"],
        department: "none"
    }
]);


// Finding documents ( Read/ Retrive)
/*
        Syntax:
        - db.collectionName.find(); - find all
        - db.collectionName.find({field: value}); - all document that wull macth the criteria
        - db.collectionName.findOne({field: value}) - first document that will match the criteria
        - db.collectionName.findOne({}) - find first document
*/

db.users.find();
db.users.find({
    "fristName": "stephen"
});
db.courses.findOne();


// Updating/ Replacing/ modifying documents (Update)
/*
    Syntax:
        db.collectionName.updateOne(
            {
                field: value
            },
            {
                $set: {
                    fieldToBeUpdated: value
                }
            }
        );

        - update the first matching document in our collection

    Multiple/Many Documents
     db.collectionName.updateMany(
            {
                criteria: value
            },
            {
                $set: {
                    fieldToBeUpdated: value
                }
            }
        );

        - update multiple documents that macthed the criteria
*/

db.users.insertOne({
    "firstName": "test",
    "lastName": "test",
    "mobileNumber": "09123456789",
    "email": "test@gmail.com",
    "company": "none"
});

db.users.updateOne({
    "firstName": "test",
}, {
    $set: {
        "firstName": "Bill",
        "lastName": "Gates",
        "mobileNumber": "123456789",
        "email": "billgates@gmail.com",
        "company": "microsoft",
        "status": "active"
    }
});




//Mini Activity
/*
	1. Make a new collection with the name "courses"
	2. Insert the following fields and values

		name: Javascript 101
		price: 5000
		description: Introduction to Javascript
		isActive: true

		name: HTML 101
		price: 2000
		description: Introduction to HTML
		isActive: true
*/

db.courses.updateOne({
    "name": "Javascript 101",
}, {
    $set: {
        "name": "Java",
        price: 6000,
        "description": "Intro to Java",
        isActive: true
    }
});


db.courses.findOne();



// Rename fieldname
db.courses.updateMany({}, {
    $rename: {
        department:
    }
});


// remove Field
db.courses.updateOne({
    "firstName": "Bill"
}, {
    $unset: {
        "status": "active"
    }
});


// Deleting Documents
/*
	Syntax:
		Deleting One Document:
			db.collectionName.deleteOne({"critera": "value"})

		Deleting Multiple Documents:
			db.collectionName.deleteMany({"criteria": "value"})

*/

db.user.deleteOne({
    "company": "Microsoft"
});



db.users.deleteMany({
    "dept": "none"
});

db.users.deleteMany({});