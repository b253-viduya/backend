// console.log("hello")

function addNum(num1, num2) {

    sum = num1 + num2;
    console.log("Displayed sum of " + num1 + " and " + num2);
    console.log(sum)
}

addNum(5, 15);

function subNum(num1, num2) {

    different = num1 - num2;
    console.log("Displayed different of " + num1 + " and " + num2);
    console.log(different)
}

subNum(20, 5);


function multiplyNum(num1, num2) {

    console.log("The product of " + num1 + " and " + num2);
    return num1 * num2;
}
let product = multiplyNum(50, 10);
console.log(product)


function divideNum(num1, num2) {
    console.log("The quotient of " + num1 + " and " + num2);
    return num1 / num2;

}
let quotient = divideNum(50, 10);
console.log(quotient)


function getCircleArea(radius) {

    const area = Math.PI * radius ** 2;
    return area.toFixed(2);;

}

const circleArea = getCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius: ");
console.log(circleArea);

function getAverage(num1, num2, num3, num4) {
    const average = (num1 + num2 + num3 + num4) / 4;
    console.log("The avarage of " + num1 + ", " + num2 + ", " + num3 + ", " + num4 + ": ")
    return average;
}

const averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar);


function checkIfPassed(score, totalScore) {
    console.log("Is 38/50 a passing score?")
    const passingPercentage = 75;
    const percentage = (score / totalScore) * 100;
    const isPassed = percentage >= passingPercentage;
    return isPassed;
}

const score = checkIfPassed(38, 50);
console.log(score);

//Do not modify
//For exporting to test.js
try {
    module.exports = {
        addNum,
        subNum,
        multiplyNum,
        divideNum,
        getCircleArea,
        getAverage,
        checkIfPassed
    }
} catch (err) {

};