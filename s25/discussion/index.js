// JSON ================================
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages hence the name JavaScript Object Notation
	- Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
	- JavaScript objects are not to be confused with JSON
	- JSON is used for serializing different data types into bytes
	- Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
                   - JavaScript Object to JSON String
	- A byte is a unit of data that is eight binary digits (1 and 0) that is used to represent a character (letters, numbers or typographic symbols)
	- Bytes are information that a computer processes to perform different tasks
	- Uses double quotes for property names
	- Syntax
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/


// // JSON objects
// {
//     "city": "Quezon City",
//     "province": "Metro Manila",
//     "country": "Philippines"

// }

// // JSON Arrays
// "cities": [{
//         "city": "Quezon City",
//         "provide": "Metro Manila",
//         "country": "Philippines"
//     },
//     {
//         "city": "Quezon City",
//         "provide": "Metro Manila",
//         "country": "Philippines"
//     },
//     {
//         "city": "Quezon City",
//         "provide": "Metro Manila",
//         "country": "Philippines"
//     }
// ]

// JSON Methods
// JSON Object contains methods for parsing and converting data into a stringified JSON data format.

// Converting Data into Stringified JSON (JS Object to JSON Data Format)
// JSON.stringify()


let batchesArr = [{
        batchName: "Batch 253"
    },
    {
        batchName: "Batch 243"
    },
]

console.log(batchesArr);

console.log("Result from stringify() method: ");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
    name: 'John',
    age: 31,
    address: {
        city: "Cebu City",
        country: "Philippines"
    }
});


console.log(data);

// let firstName = prompt("what is your First Name?");
// let lastName = prompt("what is your Lst name?");
// let age = prompt("what is your age?");
// let address = {
//     city: prompt("what is your city?"),
//     country: prompt("what is your country?"),
// };

// let otherData = JSON.stringify({
//     firstName: firstName,
//     lastName: lastName,
//     age: age,
//     address: address
// });

// console.log(otherData);


// Converting stringified JSON Data Format into JS Objects
// Deserialization
// JSON.parse()

let batchesJSON = `[
    {
        "batchName": "Batch 253"
    },
    {
        "batchName": "Batch 243"
    }
]`

console.log(batchesJSON);

// Upon receiving data, the JSON text can be converted to JS Object/s so that we can use it in out program/app.
console.log("Result from parse() method:");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
    "name": "John",
    "age": 31,
    "address": {
        "province": "Bulacan",
        "country": "Philippines"
    }
}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));