// 3
fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json()).then((json) => console.log(json));

// 4
fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(data => {
        const titles = data.map(item => item.title);
        console.log(titles);
    })
    .catch(error => console.error(error));


// 5
fetch("https://jsonplaceholder.typicode.com/todos/1")
    .then((response) => response.json()).then((json) => console.log(json));


// 6
fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then(response => response.json())
    .then(data => {
        console.log(`Title: ${data.title}, Status: ${data.completed}`);
    })
    .catch(error => console.error(error));


// 7
fetch('https://jsonplaceholder.typicode.com/todos', {
        method: 'POST',
        body: JSON.stringify({
            title: 'Buy groceries',
            completed: false
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    .then((response) => response.json()).then((json) => console.log(json));

// 8
fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: 1,
            title: 'Buy groceries',
            completed: true,
            description: 'Milk, eggs, bread, and cheese',
            date_completed: '2023-03-02',
            userId: 1
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    .then((response) => response.json()).then((json) => console.log(json));

// 9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: 1,
            title: 'Buy groceries',
            completed: true,
            description: 'Milk, eggs, bread, and cheese',
            date_completed: '2023-03-02',
            Status: "test",
            userId: 1
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    .then((response) => response.json()).then((json) => console.log("answer in number 9", json));


//10 and 11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PATCH',
        body: JSON.stringify({
            completed: true,
            date_completed: '2023-03-02'
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    .then((response) => response.json()).then((json) => console.log("answer in number 10 and 11", json));

// 12
fetch('https://jsonplaceholder.typicode.com/todos/201', {
        method: 'DELETE'
    })
    .then(response => {
        if (response.ok) {
            console.log('Item deleted successfully');
        } else {
            console.error('Failed to delete item');
        }
    })
    .catch(error => console.error(error));