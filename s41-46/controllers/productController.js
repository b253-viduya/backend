const Product = require("../models/Product");
const User = require("../models/User");

module.exports.addProduct = (reqBody) => {

    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });

    return newProduct.save().then(product => true)
        .catch(err => false);
};

// Retrieve all products
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => result)
        .catch(err => err);
};

// Retrieve all active products
module.exports.getAllActive = () => {
    return Product.find({
            isActive: true
        }).then(result => result)
        .catch(err => err);
};

// Retrieve single product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    }).catch(err => err);

};


// Update Product information (Admin only)
module.exports.updateProduct = (reqParams, reqBody) => {

    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    };

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
        .then(product => true).catch(err => err);
};


module.exports.archiveProduct = (reqParams) => {

    let updateActiveField = {
        isActive: false
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(product => true).catch(err => err);
};

module.exports.activateProduct = (reqParams) => {

    let updateActiveField = {
        isActive: true
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(product => true).catch(err => err);
};