let feelings;


console.log(feelings);


let hello;

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion. Best practices in naming variables:

		1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

			let firstName = "Michael"; - good variable name
			let pokemon = 25000; - bad variable name
        2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

			let FirstName = "Michael"; - bad variable name
			let firstName = "Michael"; - good variable name
        3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

			let first name = "Mike";

		camelCase is when we have first word in small caps and the next word added without space but is capitalized:

			lastName emailAddress mobileNumber Underscores sample:

		let product_description = "lorem ipsum"
		let product_id = "250000ea1000"

*/

let productName = "desktop computer"
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

let friend = "Owen"

friend = "Blessing"; // re-assigned/re-declared variable;

console.log(friend);


//while const cannot be updated or re-declared
// Values of constants cannot be changed and will simply return an error
console.log(interest);

let supplier;

supplier = "John Smith Tradings";

// This is considered as reassignment because its initia; value was already declared
supplier = "Zuitt Store";

// Constant cannot be declared without initialization
const pi = 3.14159
console.log(pi);


//There are issues associated with variables declared with var, regarding with hoisting.
//In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.
//Hoisting is JavaScript's default behavior of moving declarations to the top.

var pokemon = "Pikachu";
console.log(pokemon);

// a = 5;
// console.log(a);
// let a;

// let outerVar = "hello from the outside";

// {
//     let innerVar = "hello from inside"
// };

// console.log(outerVar)
// console.log(innerVar)


// [SECTION] Dara Types

// Strings
// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in JavaScript can be written using either a single (') or double (") quote
// In other programming languages, only the double quotes can be used for creating strings

let country = 'philippines'
let province = "manila"
// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

console.log("My name is Jeroxzi Viduya." + " " + greeting)

let mailAddress = "Metro Manila \n\n Philippines";
console.log(mailAddress)

let message = "John's employee went home early today"
console.log(message);

message = 'John\'s employee went home early today.'
console.log(message);

// NUMBERS
let headcount = 26;

console.log(headcount);

// Decimal number/float/fractions
let grade = 98.7;
console.log(grade);

// Euler's number
let planetDistance = 2e10;
console.log(planetDistance);

// data type number iwll become a string once it is combined/concatenated with a string.
console.log("John's grade last quarter is " + grade);

// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our application respond to certain scenarios

let isMarried = false;
let isGoodConduct = true;

console.log(isMarried)
console.log("isGoodConduct: " + isGoodConduct)

// ARRAYS
// Arrays are special kind of data type that's used to store multiple values
// Arrays can store different data types BUT is normally used to store SIMILAR data types
/*

syntax:
let/const arrayName = [elementA, elementB, elementC, ....];

*/

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
// storing different data types inside an array is not recommended because it will not make sense to in the context of programming

let details = ["john", "smith", 32, true]
console.log(details);

// OBJECTS
// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create complex data that contains pieces of information that are relevant to each other
// Every individual piece of information is called a property of the object
/*
 
    Syntax:
    let/const objectName = {
            propertyA: value,
            propertyB: value,
            ...
    }
*/

let person = {
    //key-value pair
    fullName: "Bilbo Baggins",
    age: 90,
    isMarried: false,
    contact: ["09123456789", "12345678"],
    address: {
        houseNumber: '123',
        city: "Shire"
    }
};

console.log(person);

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 91.1,
    fourthGrading: 89.1,
}

console.log(myGrades);

//typeof operator is used to determine the type data or the value of a variable. its output is a string.

console.log(typeof myGrades);

// Note: Arrayis a special type of object with methods and functions to manipulate it.
console.log(typeof grades);

const anime = ["Madoka Magica", "Parasyte", "One peiece"];
// anime = ["bungou stray dogs"];

anime[0] = "kimetsu no yaiba";
console.log(anime);

// NULL
// It is used to intentionally express the absence of a value in a variable declaration/initialization
// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified

let spouse = null,
    myNumber = 0,
    myString = '';

// Using null compared to a 0 value and an empty string is much better for readability purposes
// null is also considered as a data type of it's own compared to 0 which is a data type of a number and single quotes which are a data type of a string
console.log(spouse);
console.log(myNumber);
console.log(myString);

//UNDEFINED
// Represents the start of a variable that has been declared but without an assigned value

console.log("Feeling is" + feelings);

// Undefined vs Null
// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any value/amount
// Certain processes in programming would often return a "null" value when certain tasks results to nothing

let