// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// 4. Create an object called trainer using object literals


// 5. Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method

let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charmander", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function () {
        return "Pikachu! I choose you!";
    }
};

console.log(trainer);

console.log("Result of dot notation:")
console.log(trainer.name);

console.log("Result of square bracket notation:")
console.log(trainer["pokemon"]);

console.log("Result of talk method")
console.log(trainer.talk());




// 9. Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function (targetPokemon) {
        targetPokemon.health -= this.attack;
        console.log(`${this.name} tackled ${targetPokemon.name}.`);
        console.log(`${targetPokemon.name}'s health is now reduced to ${targetPokemon.health}.`);
        if (targetPokemon.health <= 0) {
            this.faint(targetPokemon);
        }
    };

    this.faint = function (targetPokemon) {

        console.log(`${targetPokemon.name} has fainted.`);
    };

}

let Pikachu = new Pokemon("Pikachu", 12);
let Geodude = new Pokemon("Geodude", 8);
let Mewtwo = new Pokemon("Mewtwo", 100);

console.log(Pikachu);
console.log(Geodude);
console.log(Mewtwo);

Geodude.tackle(Pikachu);
console.log(Pikachu);

Mewtwo.tackle(Geodude);
console.log(Geodude);



//Do not modify
//For exporting to test.js
try {
    module.exports = {
        trainer,
        Pokemon
    }
} catch (err) {

}