// console.log("hello")

// [SECTION] Arithmetic Operators
let x = 81,
    y = 9;

let sum = x + y;

console.log("REsult of addition operators: " + sum);

let difference = x - y;

console.log("REsult of Subtraction operators: " + difference);

let product = x * y;

console.log("REsult of Multiplication operators: " + product);

let quotient = x / y;

console.log("REsult of Division operators: " + quotient);

let remainder = x % y;

console.log("REsult of modulo operators: " + remainder);

// [SECTION] assignment Operators

// Basic Assignment Operator (=)
// The assignment operator assigns the value of the **right** operand to a variable.

let assignmentNumber = 8;

// Addition Assignment Operator
// assignmentNumber = assignmentNumber + 2;

assignmentNumber += 2; // shorthand
console.log("Result of addition assignment operator: " + assignmentNumber);


/*

        miniActivity
        1.Use assignment operators to other arithmartic operators.
        2. console log the result
        3. Send ScreenShots to our Hangouts. (code and)

*/

assignmentNumber -= 2; // shorthand
console.log("Result of Subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2; // shorthand
console.log("Result of Multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2; // shorthand
console.log("Result of Division assignment operator: " + assignmentNumber);



/*

    JS follows the PEMDAS rule:
        1. 3 * 4 = 12
        2. 12/ 5 = 2.4
        3. 1 + 2 = 3
        4. 3 - 2.4 = 0.6

*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas: " + mdas)

let pemdas = 1 + (2 - 3) * (4 / 5);

console.log("Result of pemdas: " + pemdas)


// Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied

let z = 1;

let increment = ++z; // pre increment (no need for reassignment)
console.log("Result of post-increment: " + increment)
console.log("Result of increment: " + z)

increment = z++; //post-increment
console.log("Result of post-increment: " + increment)
console.log("Result of increment: " + z)

let decrement = --z; // pre decrement
console.log("Result of post-increment: " + increment)
console.log("Result of increment: " + z)

decrement = z--; //post-decrement
console.log("Result of post-decrement: " + increment)
console.log("Result of decrement: " + z)


// [SECTION] type Coercion
/*
            - Type coercion is the automatic or implicit conversion of values from one data type to another
            - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
            - Values are automatically converted from one data type to another in order to resolve operations
*/

let numA = '10';
let numB = 12;
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16,
    numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion)

let numE = true + 1;
console.log(numE)

let numF = false + 1;
console.log(numF);

//[SECTION] Comparison Operators

//Equality Operator (==)
/* 
    - Checks whether the operands are equal/have the same content
    - Attempts to CONVERT AND COMPARE operands of different data types
    - Returns a boolean value
*/


let juan = 'juan';

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log("juan" == "juan");
console.log(juan == "juan");

// Inequality operator ( != )
/* 
            - Checks whether the operands are not equal/have different content
            - Attempts to CONVERT AND COMPARE operands of different data types
*/

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log("juan" != "juan");
console.log(juan != "juan");

// Strict Equality Operator ( === )
/* 
            - Checks whether the operands are equal/have the same content
            - Also COMPARES the data types of 2 values
            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            - Some programming languages require the developers to explicitly define the data type stored in variables to Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
            - Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/

console.log(1 === 1); // true
console.log(1 === 2); // false
console.log(1 === '1'); //false
console.log(0 === false); //false
console.log("juan" === "juan"); //true
console.log(juan === "juan"); //true


// Strict Inequality Operator
/* 
        - Checks whether the operands are not equal/have the same content
        - Also COMPARES the data types of 2 values
*/

console.log(1 !== 1); // False
console.log(1 !== 2); // True
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log("juan" !== "juan"); //false
console.log(juan !== "juan"); //false


// [SECTION] Relational Operators
//Some comparison operators check whether one value is greater or less than to the other value.

let a = 50;
let b = 65;

// Greater than operator or GT (>)
let isGreaterThan = a > b;

//Less than or LT Operator (<)
let isLessThan = a < b;

// GTE (>=)
let isGTorThan = a >= b;

// LTE (<=)
let isLTorEqual = a <= b;


console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorThan);
console.log(isLTorEqual);

console.log(a > '30'); // forced coercion to change the string to a number
console.log(a > 'twenty');

let str = 'forty';
console.log(b >= str); //Nan - not a number


// [SECTION] logical Operators
let isLegalAge = true;
let isRegistered = false;


//Logical AND oeprator (&& - double ampersand)
// Returns true if all operands are true (1 * 0 = 0)
let areAllRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + areAllRequirementsMet);

// Logical OR oepraor ( || - double pipe)
// Returns true if one of the operands are true ( 1 + 0 = 1)
let areSomeRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + areSomeRequirementsMet);

// logical Not Operator (! - exclamation point)
// Return the opposit value
let areSomeRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT Operator: " + areSomeRequirementsNotMet);