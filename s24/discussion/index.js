/* ES6 UPDATES */

// Exponent Operator ================

const firstNum = 8 ** 2; // ES6 update
console.log(firstNum);

const secondNum = Math.pow(8, 2); // old
console.log(secondNum);


// Template literals
/*
    - Allows to write strings without using the concatenation operator (+)
    - Greatly helps with code readability

*/

let name = "John";

// Pre-template literal
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals: " + message);

// Strings using Template Literals
// Uses backticks (``)

message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: " ${message}`);

// Multi-line using template literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the soulution of ${firstNum}.`;

console.log(anotherMessage);


/*
			- Template literals allow us to write strings with embedded JavaScript expressions
			- expressions are any valid unit of code that resolves to a value
			- "${}" are used to include JavaScript expressions in strings using template literals
*/


const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${ principal * interestRate}`);


// Array Destructuring =======
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability

        Syntax:
            let/const [variableName1, variableName2, variableName3] = arrayName;
*/


const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Helo ${fullName[0]} ${fullName[1]} ${fullName[2]}! it's nice to meet you!`);


// Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Helo ${firstName} ${middleName} ${lastName}! it's nice to meet you!`);


// Object Destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects

    Syntax:
        let/const {propertyName, propertyName, propertyName} = objectName;
*/

const person = {
    givenName: "Juana",
    maidenName: "Dela",
    familyName: "Cruz"
};

// Pre-object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again! `)


// Object Desctructuring

// Variable deconstructing
const {
    givenName,
    maidenName,
    familyName
} = person;

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again! `)

/*
    Passing the values using destructured properties of the object (function)
    Syntax:
        function funcName ({propertyName,propertyName,propertyName}) {
            return statement (parameters)
        }

        funcName(object)
*/

// Function destructuring
function getFullName({
    givenName,
    maidenName,
    familyName
}) {
    console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);


// Arrow Function =============
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
    - Arrow functions also have implicit return which means we don't need to use the "return" keyword
    */

// function printFullName(firstName, lastName) {
//     console.log(`${firstName} ${lastName} `);
// }
// printFullName("John", 'Smith');;


// Arrow Function (=>)
/* 
    Syntax:
        let/const variableName = (parameterA, paramterB) => {
            statement
        }

        or (If single-line no need for curl brackets)

        let/const variableName = () => expression;
*/

const printFullName = (firstName, lastName) => {
    console.log(`${firstName} ${lastName} `);
}

printFullName("Jane", "Smith");

const hello = () => {
    console.log("hellosda");
}

hello();

const sum = (x, y) => x + y;

let total = sum(1, 1);
console.log(total)


person.talk = () => "Hello";

console.log(person.talk());


// Function with default Argument Value
const greet = (name = "User") => {
    return `Good morning, ${name}!`
};

console.log(greet());
console.log(greet("Ariana"));


// Class-Based Object Blueprints
/*
    Allows the creation/instantation of objects using classes as blueprints 

    Syntax:
        class className {
            constructor (objectPropertyA, objectPropertyB){
                this.objectPropertyA = objectPropertyA;
                this.objectPropertyB = objectPropertyB;
            }
        }
*/

class Car {
    constructor(brand, model, year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
    }
}

const myCar = new Car();
console.log(myCar);

// Values of properties may be assigned after creation/instantaition of an object
myCar.brand = "Ford";
myCar.model = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);