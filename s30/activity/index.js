db.fruits.insertMany([{
        name: "Apple",
        color: "Red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Philippines", "US"]
    },

    {
        name: "Banana",
        color: "Yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Philippines", "Ecuador"]
    },

    {
        name: "Kiwi",
        color: "Green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US", "China"]
    },

    {
        name: "Mango",
        color: "Yellow",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Philippines", "India"]
    }
]);


//// 2

db.fruits.aggregate([{
        $match: {
            onSale: true
        }
    },
    {
        $count: "FruitsOnSale"
    }
])

//// 3

db.fruits.aggregate([{
        $match: {
            stock: {
                $gt: 20
            }
        }
    },
    {
        $count: "enoughStock"
    }
])

//// 4
db.fruits.aggregate([{
        $match: {
            onSale: true
        }
    },
    {
        $group: {
            _id: "$supplier_id",
            avg_price: {
                $avg: "$price"
            }
        }
    }
])


//// 5
db.fruits.aggregate([{
        $match: {
            supplier_id: {
                $exists: true
            }
        }
    },
    {
        $group: {
            _id: "$supplier_id",
            max_price: {
                $max: "$price"
            }
        }
    }
]);

//// 6
db.fruits.aggregate([{
    $group: {
        _id: "$supplier_id",
        min_price: {
            $min: "$price"
        }
    }
}])


/* reference links

		https://docs.mongodb.com/manual/reference/operator/aggregation/project/
	MongoDB $sort

		https://docs.mongodb.com/manual/reference/operator/aggregation/sort/
	MongoDB $unwind

		https://docs.mongodb.com/manual/reference/operator/aggregation/unwind/
	MongoDB $count

		https://docs.mongodb.com/manual/reference/operator/aggregation/count/
	MongoDB $avg
    
		https://docs.mongodb.com/manual/reference/operator/aggregation/avg/
	MongoDB $min



		https://docs.mongodb.com/manual/reference/op

*/