// ==================================================================
// FOR LOOP
/*
		- A for loop is more flexible than while and do-while loops. It consists of three parts:
		    1. The "initialization" value that will track the progression of the loop.
		    2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
		    3. The "finalExpression" indicates how to advance the loop.

            Syntax:

                for(initialization/initial value; expression/ condition; iteration){
                    statement;
                }

	*/

/*
		    - Will create a loop that will start from 0 and end at 20
		    - Every iteration of the loop, the value of count will be checked if it is equal or less than 20
		    - If the value of count is less than or equal to 20 the statement inside of the loop will execute
		    - The value of count will be incremented by one for each iteration
		*/

// for (let n = 0; n <= 20; n++) {
//     // The current value of n is printed out
//     console.log(n);
// }

// let myString = "IAmAdeveloper";

// console.log(myString.length);

// // console.log(myString[0]);
// // console.log(myString[1]);
// // console.log(myString[2]);
// // console.log(myString[3]);

// for (let x = 0; x < myString.length; x++) {
//     console.log(myString[x]);
// }

/*
        MINI-ACTIVTY

        Create a for loop that will use your given name as values and lenght and count the vowels
*/

// const name = "Jeroxzi Viduya";
// let vowelCount = 0;

// for (let i = 0; i < name.length; i++) {
//     const letter = name[i].toLowerCase();
//     if (letter === 'a' || letter === 'e' || letter === 'i' || letter === 'o' || letter === 'u') {
//         vowelCount++;
//     }
// }

// console.log(`Your name "${name}" has ${vowelCount} vowels.`);

// let myName = "jeroxzi";
// let vowel = 1;




// for (let i = 0; i < myName.length; i++) {
//     // If the character of your name is a vowel letter, instead of displaying the character, display "*"
//     // The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match
//     if (myName[i].toLocaleLowerCase() == 'a' || myName[i].toLocaleLowerCase() == 'e' || myName[i].toLocaleLowerCase() == 'i' || myName[i].toLocaleLowerCase() == 'o' || myName[i].toLocaleLowerCase() == 'u') {
//         // If the letter in the name is a vowel, it will print the *
//         console.log("*");
//     } else {
//         // Print in the console all non-vowel characters in the name
//         console.log(myName[i]);
//     }
// }


// for (let count = 0; count <= 20; count++) {
//     if (count % 2 === 0) {
//         continue;
//     }

//     console.log("Continue and Break: " + count)

//     if (count > 10) {
//         break;
//     }
// }

let name = "alejandro";

for (let i = 0; i < name.length; i++) {
    console.log(name[i]);

    if (name[i].toLowerCase() === "a") {
        console.log("Continue to the next iterration");
        continue;
    }

    if (name[i] == "d") {
        break;
    }
}