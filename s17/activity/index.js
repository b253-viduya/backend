// console.log("hello World")

/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
//first function here:

function printUserInfo() {

    let fullname = "John Doe";
    let age = "25";
    let location = "123 street, Quezon city";
    let OtherInformation = ["joe", "danny"];


    console.log("Hello, I'm " + fullname + ".");
    console.log("I am " + age + " years old.");
    console.log("I live in " + location);
    console.log("I have a cat named " + OtherInformation[0] + ".");
    console.log("I have a dog Danny " + OtherInformation[1] + ".");

}

printUserInfo();

/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
//second function here:

function printFiveBands() {
    console.log("The Beatles");
    console.log("Taylot Swift");
    console.log("The Eagles");
    console.log("Rivermaya");
    console.log("Eraserheads");
}
printFiveBands();


/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/

function printFiveMovies() {
    console.log("Lion King");
    console.log("Howl's Moving Castle");
    console.log("Meet the Robinsons");
    console.log("School of Rock");
    console.log("Spritted Away");
}

//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends() {
    let friend1 = "Eugene";
    let friend2 = "Dennis";
    let friend3 = "Vincent";

    console.log("These are my friends:");
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
};

printFriends();

// console.log(friend1);
// console.log(friend2);



//Do not modify
//For exporting to test.js
try {
    module.exports = {
        printUserInfo,
        printFiveBands,
        printFiveMovies,
        printFriends
    }
} catch (err) {

}